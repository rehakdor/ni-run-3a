#ifndef CODE_VECTOR_H
#define CODE_VECTOR_H

#include <vector>
#include <cstdint>

// Holds all the code
class CodeVector{
protected:
    std::vector<std::uint8_t> code;

public:
    // this is used to read the code
    const std::uint8_t *get(size_t offset) const;

    // this is used to append the code
    void extend(size_t length);
    std::uint8_t *getWritable(size_t offset);
    size_t size(void) const;
};

#endif
