#include "vm.hpp"

#include "exception.hpp"
#include "util.hpp"

#include <cstdint>
#include <memory>
#include <fstream>
#include <iostream>
#include <cstring>
#include <algorithm>

enum Tags{
    Integer = 0x00,
    Boolean = 0x06,
    Null = 0x01,
    String = 0x02,
    Method = 0x03
};

enum Opcodes{
    Literal = 0x01,
    Print = 0x02,
    Drop = 0x10
};

#define opcodeArgSize_entry(name, size) \
case Opcodes::name: \
    return size
size_t VM::opcodeArgSize(uint8_t opcode){
    switch(opcode){
        opcodeArgSize_entry(Literal, 2);
        opcodeArgSize_entry(Print, 3);
        opcodeArgSize_entry(Drop, 0);
    }
    throw InvalidBytecode("Unknown opcode size");
}
CPOInteger *VM::loadCPOInteger(std::ifstream& file){
    int32_t value;
    file.read((char *)&value, sizeof(value));
    value = LE2NATIVE32(value);
    return new CPOInteger(value);
}
CPOBoolean *VM::loadCPOBoolean(std::ifstream& file){
    uint8_t value;
    file.read((char *)&value, sizeof(value));
    return new CPOBoolean(value);
}
CPOString *VM::loadCPOString(std::ifstream& file){
    uint32_t length;
    file.read((char *)&length, sizeof(length));
    length = LE2NATIVE32(length);
    std::unique_ptr<char[]> data(new char[length]);
    file.read(data.get(), length);
    return new CPOString(std::string(data.get(), length));
}
CPOMethod *VM::loadCPOMethod(std::ifstream& file){
    uint16_t name_index;
    uint8_t argument_count;
    uint16_t local_count;
    uint32_t instruction_count;

    file.read((char *)&name_index, sizeof(name_index));
    name_index = LE2NATIVE16(name_index);
    file.read((char *)&argument_count, sizeof(argument_count));
    file.read((char *)&local_count, sizeof(local_count));
    local_count = LE2NATIVE16(local_count);
    file.read((char *)&instruction_count, sizeof(instruction_count));
    instruction_count = LE2NATIVE32(instruction_count);

    // compute code length
    std::streampos start = file.tellg();
    for(size_t i = 0; i < instruction_count; i++){
        uint8_t opcode;
        file.read((char *)&opcode, sizeof(opcode));
        file.seekg(opcodeArgSize(opcode), std::ios_base::cur);
    }

    // insert into code vector
    size_t length = file.tellg() - start;
    file.seekg(start, std::ios_base::beg);
    size_t code_vector_start = code_vector.size();
    code_vector.extend(length);
    file.read((char *)code_vector.getWritable(code_vector_start), length);

    return new CPOMethod(name_index, argument_count, local_count, start, length);
}
#define loadCPO_entry(arg) \
case Tags::arg: \
    return loadCPO##arg(file)
CPO *VM::loadCPO(std::ifstream& file){
    uint8_t tag;
    file.read((char *)&tag, sizeof(tag));
    switch(tag){
        loadCPO_entry(Integer);
        loadCPO_entry(Boolean);
        loadCPO_entry(String);
        loadCPO_entry(Method);
        case Tags::Null:
            return new CPONull;
    }
    throw InvalidBytecode("Unknown tag");
}
void VM::loadConstantPool(std::ifstream& file){
    uint16_t cp_size;
    file.read((char *)&cp_size, sizeof(cp_size));
    cp_size = LE2NATIVE16(cp_size);
    for(size_t i = 0; i < cp_size; i++)
        constant_pool.addObject(*loadCPO(file));
}



unsigned int VM::CPO2RTO(const CPO& cpo){
    switch(cpo.getType()){
        case CPO::Type::Null:
            return heap.addObject(*new RTONull);
        case CPO::Type::Boolean:{
            const CPOBoolean& real_cpo = static_cast<const CPOBoolean&>(cpo);
            return heap.addObject(*new RTOBoolean(real_cpo.value));
        }
        case CPO::Type::Integer:{
            const CPOInteger& real_cpo = static_cast<const CPOInteger&>(cpo);
            return heap.addObject(*new RTOInteger(real_cpo.value));
        }
        default:
            throw InvalidBytecode("Requested to create an impossible real-time object from a constant pool object");
    }
}
void VM::runLiteral(void){
    uint16_t index;
    std::memcpy(&index, ip, sizeof(index));
    ip += sizeof(index);
    index = LE2NATIVE16(index);
    const CPO& cpo = constant_pool.getObject(index);
    operand_stack.push(CPO2RTO(cpo));
}
void VM::printRTO(const RTO& rto){
    switch(rto.getType()){
        case RTO::Type::Null:
            std::cout << "null";
            break;
        case RTO::Type::Boolean:
            std::cout << (static_cast<const RTOBoolean&>(rto).value ? "true" : "false");
            break;
        case RTO::Type::Integer:
            std::cout << static_cast<const RTOInteger&>(rto).value;
            break;
    }
}
#define BACKLASH_ITEM(input, output) \
    case input: \
    std::cout << output; \
    break
void VM::runPrint(void){
    uint16_t format_index;
    uint8_t argument_count;
    std::memcpy(&format_index, ip, sizeof(format_index));
    ip += sizeof(format_index);
    format_index = LE2NATIVE16(format_index);
    std::memcpy(&argument_count, ip, sizeof(argument_count));
    ip += sizeof(argument_count);
    const CPO& format = constant_pool.getObject(format_index);
    if(format.getType() != CPO::Type::String)
        throw InvalidBytecode("Print format object is not of type string");
    const CPOString& real_format = static_cast<const CPOString&>(format);
    std::vector<unsigned int> arguments;
    for(size_t i = 0; i < argument_count; i++)
        arguments.push_back(operand_stack.pop());
    std::reverse(arguments.begin(), arguments.end());

    // This whole block is largely taken from homework 2A
    const char *c_str = real_format.value.c_str();
    size_t i = 0;
    size_t curr_arg = 0;
    bool backlash_mode = false;
    for(char curr_char; (curr_char = c_str[i]) != '\0'; i++){
        if(backlash_mode){
            switch(curr_char){
                BACKLASH_ITEM('~', "~");
                BACKLASH_ITEM('n', std::endl);
                BACKLASH_ITEM('"', "\"");
                BACKLASH_ITEM('t', "\t");
                BACKLASH_ITEM('\\', "\\");
                case 'r':
                    break; // doesnt work in C++
                default:
                    throw InvalidBytecode("Unknown \\ operator");
            }
            backlash_mode = false;
        }else{
            if(curr_char == '\\')
                backlash_mode = true;
            else if(curr_char == '~'){
                const RTO& rto = heap.getObject(arguments[curr_arg]);
                printRTO(rto);
                heap.release(arguments[curr_arg]); // decrease RTO refcounter
                curr_arg++;
            }else
                std::cout << curr_char;
        }
    }
    if(backlash_mode)
        throw InvalidBytecode("Print cannot end with \\");

    // put the null RTO on stack
    operand_stack.push(heap.addObject(*new RTONull));
}
void VM::runDrop(void){
    heap.release(operand_stack.pop());
}
#define runAt_entry(arg) \
case Opcodes::arg: \
    run##arg(); \
    break;
void VM::runIP(void){
    while(ip < code_vector.get(code_vector.size())){
        uint8_t opcode = *ip;
        ip++;
        switch(opcode){
            runAt_entry(Literal);
            runAt_entry(Print);
            runAt_entry(Drop);
            default:
                throw InvalidBytecode("Unknown opcode");
        }
    }
}



VM::~VM(void){
    // items left on operand stack after program has finished (incorrect bytecode)
    // TODO should this be silent or error?
    while(operand_stack.size() != 0){
        unsigned int pointer = operand_stack.pop();
        heap.release(pointer);
    }
}



void VM::loadFile(std::ifstream& file){
    loadConstantPool(file);
    // WARNING: this is temporary
    entry = 0;
}
void VM::run(void){
    ip = code_vector.get(entry);
    runIP();
}
