#include "vm.hpp"
#include "exception.hpp"

#include <iostream>
#include <fstream>
#include <exception>

int main(int argc, char *argv[]){
    if(argc < 2)
        std::cerr << "Missing argument" << std::endl;

    try{
        std::ifstream input_file(argv[1]);
        input_file.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);

        VM vm;
        vm.loadFile(input_file);
        input_file.close();

        vm.run();
    }catch(InvalidBytecode& e){
        std::cerr << "Invalid bytecode: " << e.what() << std::endl;
    }catch(std::exception& e){
        std::cerr << e.what() << std::endl;
        goto fail;
    }catch(...){
        std::cerr << "Unknown object thrown" << std::endl;
        goto fail;
    }

    return 0;

    fail:
    std::cout << "Failure" << std::endl;
    return 1;
}
