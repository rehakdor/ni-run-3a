#ifndef HEAP_H
#define HEAP_H

#include <cstdint>
#include <map>

class Heap;

// Run-Time Object base
class RTO{
public:
    enum class Type{
        Null,
        Boolean,
        Integer
    };

    // reference counter
    unsigned int refcount = 1;
    void take(void);

    virtual ~RTO(void) = default;
    virtual Type getType(void) const = 0;
};
class RTONull: public RTO{
public:
    Type getType(void) const override;
};
class RTOBoolean: public RTO{
public:
    bool value;

    RTOBoolean(bool pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class RTOInteger: public RTO{
public:
    int32_t value;

    RTOInteger(int32_t pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};

class Heap{
protected:
    unsigned int next_pointer = 0;
    std::map<unsigned int, RTO *> objects;

    void removeObject(unsigned int pointer);

public:
    RTO& getObject(unsigned int pointer);
    unsigned int addObject(RTO& new_object);

    void take(unsigned int pointer);
    void release(unsigned int pointer);
};

#endif
