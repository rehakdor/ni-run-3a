#include "operand_stack.hpp"

#include <vector>

unsigned int OperandStack::pop(void){
    unsigned int result = stack.back();
    stack.pop_back();
    return result;
}
void OperandStack::push(unsigned int pointer){
    stack.push_back(pointer);
}
size_t OperandStack::size(void){
    return stack.size();
}
