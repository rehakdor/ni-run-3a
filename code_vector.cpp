#include "code_vector.hpp"

#include <cstdint>
#include <vector>
#include <cstring>

const std::uint8_t *CodeVector::get(size_t offset) const{
    return code.data() + offset;
}
void CodeVector::extend(size_t length){
    code.resize(code.size() + length);
}
std::uint8_t *CodeVector::getWritable(size_t offset){
    return code.data() + offset;
}

size_t CodeVector::size(void) const{
    return code.size();
}
