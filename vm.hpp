#ifndef VM_H
#define VM_H

#include "constant_pool.hpp"
#include "heap.hpp"
#include "operand_stack.hpp"
#include "code_vector.hpp"

#include <fstream>
#include <cstdint>

class VM{
protected:
    // returns remaining size of an instruction after an opcode was read
    size_t opcodeArgSize(uint8_t opcode);
    // these methods create constant pool objects
    CPOInteger *loadCPOInteger(std::ifstream& file);
    CPOBoolean *loadCPOBoolean(std::ifstream& file);
    CPOString *loadCPOString(std::ifstream& file);
    CPOMethod *loadCPOMethod(std::ifstream& file);
    CPO *loadCPO(std::ifstream& file);
    void loadConstantPool(std::ifstream& file);

    // creates a runtime object from a constant pool object, inserts it in heap, returns pointer to heap
    unsigned int CPO2RTO(const CPO& cpo);
    // runs instruction Literal
    void runLiteral(void);
    // helper function of runPrint; prints a runtime object to standard output
    void printRTO(const RTO& rto);
    // runs instruction Print
    void runPrint(void);
    // runs instruction Drop
    void runDrop(void);
    // runs instruction pointer until the end of code vector
    void runIP(void);

public:
    ConstantPool constant_pool;
    Heap heap;
    OperandStack operand_stack;
    CodeVector code_vector;
    uint16_t entry;
    const uint8_t *ip;

    ~VM(void);

    void loadFile(std::ifstream& file);
    void run(void);
};

#endif
