#include "heap.hpp"

#include "util.hpp"

void RTO::take(void){
    refcount++;
}

RTO::Type RTONull::getType(void) const{
    return Type::Null;
}
RTO::Type RTOBoolean::getType(void) const{
    return Type::Boolean;
}
RTO::Type RTOInteger::getType(void) const{
    return Type::Integer;
}

RTO& Heap::getObject(unsigned int pointer){
    auto it = objects.find(pointer);
    MY_ASSERT(it != objects.end());
    return *it->second;
}
unsigned int Heap::addObject(RTO& new_object){
    objects.emplace(next_pointer, &new_object);
    return next_pointer++;
}
void Heap::removeObject(unsigned int pointer){
    MY_ASSERT(objects.erase(pointer) == 1);
}


void Heap::take(unsigned int pointer){
    getObject(pointer).take();
}
void Heap::release(unsigned int pointer){
    RTO& rto = getObject(pointer);
    rto.refcount--;
    if(rto.refcount == 0){
        removeObject(pointer);
        delete &rto;
    }
}
