.PHONY: all clean
.SUFFICES:

CXX = clang++
CXXFLAGS ?= -O3 -Wall -pedantic -g
CXXFLAGS_BASE := -std=c++11 -fno-strict-aliasing

SRC := constant_pool heap operand_stack code_vector vm main
OBJ := $(addsuffix .o,$(SRC))
EXE := main

all: $(EXE)
clean:
	-rm $(EXE) $(OBJ)

constant_pool.o: constant_pool.hpp
heap.o: heap.hpp util.hpp
operand_stack.o: operand_stack.hpp
code_vector.o: code_vector.hpp
vm.o: vm.hpp constant_pool.hpp heap.hpp operand_stack.hpp code_vector.hpp exception.hpp util.hpp
main.o: vm.hpp exception.hpp
%.o: %.cpp Makefile
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) -c $< -o $@
$(EXE): $(OBJ)
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) $^ $(LDFLAGS_BASE) $(LDFLAGS) -o $@
