#ifndef CONSTANT_POOL_H
#define CONSTANT_POOL_H

#include <cstdint>
#include <vector>
#include <string>

// Constant Pool Object base
class CPO{
public:
    enum class Type{
        Null,
        Boolean,
        Integer,
        String,
        Method
    };

    virtual ~CPO(void) = default;
    virtual Type getType(void) const = 0;
};
class CPONull: public CPO{
public:
    Type getType(void) const override;
};
class CPOBoolean: public CPO{
public:
    bool value;

    CPOBoolean(bool pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOInteger: public CPO{
public:
    int32_t value;

    CPOInteger(int32_t pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOString: public CPO{
public:
    std::string value;

    CPOString(const std::string& pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOMethod: public CPO{
public:
    uint16_t name_index;
    uint8_t argument_count;
    uint16_t local_count;
    uint32_t start;
    uint32_t length;

    CPOMethod(uint16_t pname_index, uint8_t pargument_count, uint16_t plocal_count,
              uint32_t pstart, uint32_t plength)
    : name_index(pname_index), argument_count(pargument_count), local_count(plocal_count),
      start(pstart), length(plength){
    }
    Type getType(void) const override;
};

class ConstantPool{
protected:
    std::vector<CPO *> objects;

public:
    ~ConstantPool(void);

    void addObject(CPO& object);
    const CPO& getObject(uint16_t index) const;
};

#endif
