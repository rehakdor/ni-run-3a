#ifndef OPERANT_STACK_H
#define OPERANT_STACK_H

#include <vector>

class OperandStack{
protected:
    std::vector<unsigned int> stack;

public:
    unsigned int pop(void);
    void push(unsigned int pointer);
    size_t size(void);
};

#endif
